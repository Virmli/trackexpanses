var express = require('express');
var router = express.Router();
 
var auth = require('./auth.js');
var users = require('./users.js');
var grid = require('./grid.js');
 
/*
	auth.js
	Routes that can be accessed by any one
*/
router.post('/login', auth.login);
router.put('/createUser', auth.createUser);
router.get('/getUsers', auth.getUsers);

/*
	users.js
*/
router.get('/user/:user_id', users.getUserData);
router.put('/updateProfile', users.updateProfile);
router.put('/secureUpdate/:id', users.secureUpdate);
router.delete('/deleteProfile', users.deleteProfile);

/*
	grid.js
*/
router.put('/createGrid', grid.createGrid);
router.post('/findAllGrids', grid.findAllGrids);
router.post('/:month/:yyyy/resetBudget', grid.resetBudget);
// router.post('/:month/:yyyy/findGrid', grid.findGrid); // DEPRECATED
router.put('/:month/:yyyy/createExpense', grid.createExpense);
router.post('/:month/:yyyy/findAllExpenses', grid.findAllExpenses);
router.post('/:month/:yyyy/sumAllExpenses', grid.sumAllExpenses);
router.post('/:expense_id/updateExpense', grid.updateExpense);
router.delete('/:expense_id/deleteExpense', grid.deleteExpense);

// router.put('/getExpense', users.getExpense);


/*
 * Routes that can be accessed only by autheticated users
 */
/*router.get('/api/v1/products', products.getAll);
router.get('/api/v1/product/:id', products.getOne);
router.post('/api/v1/product/', products.create);
router.put('/api/v1/product/:id', products.update);
router.delete('/api/v1/product/:id', products.delete);
 */
/*
 * Routes that can be accessed only by authenticated & authorized users
 */
/*router.get('/api/v1/admin/users', user.getAll);
router.get('/api/v1/admin/user/:id', user.getOne);
router.post('/api/v1/admin/user/', user.create);
router.put('/api/v1/admin/user/:id', user.update);
router.delete('/api/v1/admin/user/:id', user.delete);
 */
module.exports = router;
