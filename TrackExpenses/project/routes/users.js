//users.js
var mongo = require('mongodb');
var ObjectID = require('mongodb').ObjectID;
var express = require('express');
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var db = mongojs('Trackexpenses',["students", "expense", "monthBudget"]);
app = express();
app.use(bodyParser.json());


var users = {
/*
	add error handler
*/
	updateProfile: function(req, res) {
		var body = req.body;
		var userId = body._id;
		var o_id = new ObjectID(userId);
		delete body._id;
		var jsonBody=JSON.stringify(body);
		if(Object.keys(body).length != 0){	
			db.students.update({"_id": o_id},{$set:{"firstName": body.firstName,
													"lastName": body.lastName,
													"email": body.email,
													"dob": body.dob,
													"about": body.about}
											  },{safe:true}, function(err, records) {
				if(err){
					res.send(err);
				}else{
					res.send(records);
				}
			});
		}else{
			res.body("You not passing anything");
		}
	},
/*	
"userLogin" //secure update
"password" //secure update
*/

	secureUpdate: function(req, res){
		var o_id = new ObjectID(req.params.id);
		var body = req.body;
		db.students.update({"_id": o_id},{$set:{"password":body.password}},{safe:true}, function(err, records){
			if(err){
					res.send(err);
				}else{
					res.send(records);
				}
		});
	},

	// DELETE the user’s profile (his profile, grids, expenses)
	// http://52.27.200.202:3000/deleteProfile 
	deleteProfile: function(req, res) {
		console.log("delete");
		var o_id = new ObjectID(req.body.student_id);
		
		// find and delete all user's expenses
		db.expense.remove( { "student_id" : o_id }, function(err, records){
			if(err) {
				res.send(err);
			} else {
				console.log("...Expenses deleted");
			}
		});// close db.expense.remove

		// find and delete all user's grids
		db.monthBudget.remove( { "student_id" : o_id }, function(err, records){
			if(err) {
				res.send(err);
			} else {
				console.log("...Grids deleted");
			}
		});// close db.monthBudget.remove

		// find and delete all user's profile info
		db.students.remove( { '_id' : o_id }, function(err, records){
			if(err) {
				res.send(err);
			} else {
				console.log("Success: User Deleted!");
				res.send(records); 
			}
		});// close db.students.remove

	}, // close deleteProfile

	// GET: Retrive only one specific user with all corresponding data
	// http://52.27.200.202:3000/user/:user_id
	// or 404 if not found
	getUserData: function (req, res) {
	    var userId = req.params.user_id;

	    // Create a default mongodb id using userId
	    var o_id = new ObjectID(userId);	    
	    
	    // find the profile in mongo
	    db.students.find ( { '_id' : o_id }, function(err, records) {				
	    		console.log(records);
				if (records.length == 0) {			
					res.status(404);
					res.send('404 Not Found'); 
				} else {
					res.send(JSON.stringify(records));
				}
		});
	}

};
module.exports = users; 
