// grid.js
var mongo = require('mongodb');
var ObjectID = require('mongodb').ObjectID;
var express = require('express');
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var db = mongojs('Trackexpenses',["expense", "monthBudget"]);
app = express();
app.use(bodyParser.json());

// Insert all your routes related to a grid	
var grid = {

	// PUT: Create a new grid: set month, year, initial_budget values
	// http://52.27.200.202:3000/createGrid/
	createGrid: function(req, res) {
		// Retrieve the body
		var body = req.body; 

		// Save the body to the mongodb
		db.monthBudget.insert(body, function(err, records) {
			if (err) {
				console.log(err);
			} else {
				res.send(records); //+ "-----> Data for a new grid saved!"); 
			}
			
		});
	}, // close createGrid

	// POST: reset the initial budget value
	// http://52.27.200.202:3000/:month/:yyyy/resetBudget/
	resetBudget: function(req, res) {
		var month = req.params.month; 
		var year = req.params.yyyy;
		var student_id = req.body.student_id; 
		var newBudget = req.body.newBudget;

		db.monthBudget.update(
			{"student_id" : student_id, "month" : month, "year" : year}, 
			{$set: {"initial_budget" : newBudget}}, function (err, records){
				if (records["n"] > 0) {				
					console.log("set");
					res.send(JSON.stringify(records) + "-----> 'initial_budget' was Sucessfully Reset!"); 
				} else {
					res.status(404);
					res.send('404 Not Found'); 
				}
			}
		);
	}, // close resetBudget

	// POST: retrieve all existing grids
	// http://52.27.200.202:3000/findAllGrids
	findAllGrids: function(req, res) {
		var student_id = req.body.student_id; 

		db.monthBudget.find({"student_id" : student_id}, function (err, records) {
			if (records.length != 0) {				
				console.log("find all month grids");
				res.send(JSON.stringify(records)); 
			} else {
				res.status(404);
				res.send('404 Not Found'); 
			}
		});

	}, // close findAllGrids

	// POST: retrieve the selected grid initial budget
	// http://52.27.200.202:3000/:month/:yyyy/findGrid
	findGrid: function(req, res) {
		var month = req.params.month; 
		var year = req.params.yyyy; 
		var student_id = req.body.student_id; 

		db.monthBudget.find(
			{"student_id" : student_id, "month" : month, "year" : year}, function (err, records) {
				// res.send(JSON.stringify(records) + "-----> monthBudget document was Sucessfully Found!"); 
				if (records.length != 0) {				
					console.log("find");
					res.send(JSON.stringify(records)); 
				} else {
					res.status(404);
					res.send('404 Not Found'); 
				}
		});
	}, // close findGrid

	// PUT: Create a new expense document
	// http://52.27.200.202:3000/:month/:yyyy/createExpense
	createExpense: function (req, res) {
		req.body.month = req.params.month; 
		req.body.year = req.params.yyyy; 
		var body = req.body;

		// Check if date, name, amount were entered or no
		if (body.day == '' || body.amount == '' || body.name == '') 
			res.send("Error: All fields should be entered. Please fill the expense form.");
		
		// Check if 'amount' is the numeric time and not >= 0
		var amountInt = parseFloat(body.amount);
		// var amountInt = body.amount; 
//add ERROR HANDLING	
		if (isNaN(amountInt)) { 			
			res.send("Error: 'Amount' should be a number. Please fill the expense form.");
		}

		if (amountInt <= 0) {
			res.send("Error: 'Amount' cannot be zero. Please fill the expense form.");
		}

		// Subtract "amount" from "available_funds"
		var decreasedFunds = body.available_funds - body.amount;  
		body.available_funds = decreasedFunds;  

		// console.log(body.month);
		// console.log(body); 
		
		if(Object.keys(body).length != 0) {			
			db.expense.insert(body, function (err, records) {
	       		if(err) {
					res.send ("Error, try one more time!");
				} else {
					// res.send ("Expense created!");
					res.send(records); 
				}
			});
		} else {
			res.send("All fields should be entered. \nPlease fill the expense form.");
		}

	},// close createExpense

	// POST: Find all expense documents
	// http://52.27.200.202:3000/:month/:yyyy/findAllExpenses
	findAllExpenses: function (req, res) {
		var month = req.params.month; 
		var year = req.params.yyyy; 
		var student_id = req.body.student_id; 

		// Find the monthlyBudget amount
		var monthlyBudget; 
		db.monthBudget.find(
			{"student_id" : student_id, "month" : month, "year" : year}, function (err, records) {
				if (records.length != 0){
					monthlyBudget = records[0].initial_budget; 
			}
		});
		console.log(monthlyBudget); 

		// Find all expenses to sum all "amount" values
		var availableFunds, sumAmounts = 0;
		db.expense.find(
			{"student_id" : student_id, "month" : month, "year" : year}, // query
			{_id : 0, amount : 1}, // projection
			function (err, records) {
				if (err) res.send("Error, try one more time!");
				else {
					
					for ( var i = 0; i < records.length; i++) {
					// for ( var i = 0; i < 5; i++) {
						sumAmounts += parseFloat(records[i].amount);  //was parseInt
					}
					console.log(sumAmounts+"");
					availableFunds = monthlyBudget - sumAmounts;
					availableFunds = availableFunds + "";
					// sumAmounts = sumAmounts + ""; 
					// res.send(sumAmounts+""); 
				}
		});
		
		// db.expense.runCommand('count', function(err))

		// Find all expenses and send to the front-end
		db.expense.find(
			{"student_id" : student_id, "month" : month, "year" : year} // query
			// {_id : 0, available_funds : 1} // projection
		 
			// {"sort" : [['available_funds' , 'desc']]}
		).sort( { date : 1 } ).toArray(function (err, records){ 
			// function (err, records) {
			
				if (records.length != 0) {				
					console.log("find");
					console.log(records.length);
					//add monthly budget
					 var response = '{"monthlyBudget": '+monthlyBudget+', "availableFunds":'+availableFunds+',"month":"'+month+'","year":"'+year+'", "rows":'+JSON.stringify(records)+'}';
					//res.send(JSON.stringify(records)); 
					res.send(response);
				} else {
					var response = '{"monthlyBudget": '+monthlyBudget+', "availableFunds":'+monthlyBudget+',"month":"'+month+'","year":"'+year+'", "rows": ""}';
					//res.status(404);
					res.send(response); 
				}
		});
	}, // close findAllExpenses

	// DELETE: Delete the expense document 
	// http://52.27.200.202:3000/:expense_id/deleteExpense
	deleteExpense: function(req, res) {
		var expenseId = req.params.expense_id; 
		var o_id = new ObjectID(expenseId); 

		// find and delete an expense document
		db.expense.remove( { "_id" : o_id }, function(err, records) {
			if (records["n"] > 0) {
				console.log(records["n"]);
				res.send(records); 			
			} else {
				console.log(err);
				// console.log(records);
				res.status(404);
				res.send('404 Not Found'); 
			}
		});
	}, // close deleteExpense

	// POST : Update the expense document
	// http://52.27.200.202:3000/:expense_id/updateExpense
	updateExpense: function(req, res) {
		var expenseId = req.params.expense_id; 
		var o_id = new ObjectID(expenseId);

		var amountUpdate = req.body.amount; 
		var nameUpdate = req.body.name; 
		var detailsUpdate = req.body.details; 

		db.expense.update(
			{'_id' : o_id}, // query
			{$set: {"amount" : amountUpdate, "name" : nameUpdate, "details" : detailsUpdate}}, 
			function (err, records) {
				if(err) {
					res.send(err);
				} else {
					res.send(records);
				}
		});


	}, // close updateExpense

	// POST: Find the sum of all expenses 
	// sum all amounts
	// http://52.27.200.202:3000/:month/:yyyy/sumAllExpenses
	sumAllExpenses: function (req, res) {
		var month = req.params.month; 
		var year = req.params.yyyy; 
		var student_id = req.body.student_id; 

		db.expense.find(
			{"student_id" : student_id, "month" : month, "year" : year}, // query
			{_id : 0, amount : 1}, // projection
			
			function (err, records) {
				if (err) res.send ("Error, try one more time!");
				else {
					var sum = 0;
					// for ( var i = 0; i < records.length; i++) {
					for ( var i = 0; i < 5; i++) {
						sum += parseFloat(records[i].amount);  //Was parsInt
					}
					console.log(sum+""); 
					res.send(sum+""); 
				}
		});

	}, // close sumAllExpenses

};// close grid

module.exports = grid;

