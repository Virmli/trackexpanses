// auth.js file
var express = require('express');
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var db = mongojs('Trackexpenses',["students"]);
app = express();
app.use(bodyParser.json());

var auth = {
   login: function(req, res){
   	//simple login auth.
	var body = req.body;
	
	var userLoginCheck = false;
	if(body.userLogin != "" && body.userLogin.length >=4 && body.userLogin.length <= 15 && !(/\s/g.test(body.userLogin))) {
		userLoginCheck = true;
	}

	var passwordCheck = false;
	if(body.password != "" && body.password.length >=3 && body.password.length <= 20 && !(/\s/g.test(body.password))) {//change to 6 characters
		passwordCheck = true;
	}

	if(userLoginCheck || passwordCheck) {
		db.students.find( {userLogin: body.userLogin, password: body.password}, function(err, records){
		
			if(Object.keys(records).length != 0){
				console.log(records);
				res.send(records);
			}else{
				var data = JSON.stringify('{"msg":"No users with this login/password.", "login":"1"}');
				res.send(JSON.parse(data));
			}
		});
	}else {
		var data = JSON.stringify('{"msg":"You spelled login/password incorectly.", "login": "1"}');
		res.send(JSON.parse(data));
	}
},

// PUT: Create a new profile
// http://52.27.200.202:3000/createUser
	createUser: function(req, res) {
		// Retrieve the body
		var body = req.body; 

		if(Object.keys(body).length != 0) {	
			
			var firstLastNameRegex = /^[a-zA-Z]{2,30}$/;//add dash and search for name paterna
			var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var loginRegex = /^\S+\w{4,15}\S{1,}$/;//check the lenght of the login
			
			//password validation 
			var passwordCheck = true;
			if(body.password != "" && body.password === body["confirm-password"] && body.password.length >=6 && body.password.length <= 20 && !(/\s/g.test(body.password))) {
				passwordCheck = false;
			}

			//TODO: more custome pasword and login validation

			delete body["confirm-password"];
			console.log(body);

			var userNameCheck = true;
			if(body.userLogin != "" && body.userLogin.length >=4 && body.userLogin.length <= 15 && !(/\s/g.test(body.userLogin))) {
				userNameCheck = false;
			}

			if(!(firstLastNameRegex.test(body.firstName)) || !(firstLastNameRegex.test(body.lastName)) || userNameCheck || passwordCheck || !(emailRegex.test(body.email))){
				//which fields have conflict
				var data = '{"msg":"Some fields have errors. Press exclamation sign for more information", "firstName": "0", "lastName": "0", "email": "0", "password": "0", "username": "0"}';
				data = JSON.parse(data);
				if(!(firstLastNameRegex.test(body.firstName))){
					data.firstName = "1";
				}
				if(!(firstLastNameRegex.test(body.lastName))){
					data.lastName = "1";
				}
				if(!(emailRegex.test(body.email))){
					data.email = "1";
				}
				if(passwordCheck){
					data.password = "1";
				}
				if(userNameCheck){
					data.username = "1";
				}
				console.log("Some of fileds must not be empty");
				data = JSON.stringify(data);
				console.log(data);
				res.send(data);
			} else {
				db.students.insert(body, function(err, result) { //IMPORTANT: make error handling if login is dublicated.
		       		//console.log(err);
		       		//console.log(result);
		       		if(err){
		       			var data = JSON.stringify('{"msg":"Problems with the server or UserName already exists"}');
		       			res.send(data);
					}else{
						var data = JSON.stringify('{"msg":"User Created Successfully"}');
						res.send(JSON.parse(data));
					}
				});
			}
		}else{
			console.log("No data User creation");
			var data = JSON.stringify('{"msg":"No data. Please enter data you want to send"}');
			res.send(data);
		}
	},

// GET: Retrive all profiles
// http://52.27.200.202:3000/getUsers
	getUsers: function(req, res) {
		db.students.find(function(err, records){
			if(records != null){	
				res.send(records);
			}else{
				res.send("No records with this request");
			}
		});
	}	



}; 

module.exports = auth;
