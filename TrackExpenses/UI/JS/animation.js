$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	//side menu navigation
	$('.accountSettings').click(function(e) {
		$(".gridContainer").fadeOut(100);
		$(".secureEdit").fadeOut(100);
 		$(".userEdit").delay(100).fadeIn(100);
		$('.overview').removeClass('active');
		$('.secureSettings').removeClass('active');
		canvas.style.display="none";
		$(this).addClass('active');
		e.preventDefault();
	});
	$('.overview').click(function(e) {
		$(".userEdit").fadeOut(100);
		$(".secureEdit").fadeOut(100);
 		$(".gridContainer").delay(100).fadeIn(100);
		$('.accountSettings').removeClass('active');
		$('.secureSettings').removeClass('active');
		$(this).addClass('active');
		canvas.style.display="block";
		e.preventDefault();
	});
		$('.secureSettings').click(function(e) {
		$(".userEdit").fadeOut(100);
		$(".gridContainer").fadeOut(100);
 		$(".secureEdit").delay(100).fadeIn(100);
		$('.accountSettings').removeClass('active');
		$('.overview').removeClass('active');
		canvas.style.display="none";
		$(this).addClass('active');
		e.preventDefault();
	});
		////FLOAT LEFT SIDE navigation
$('.container').scroll(function() { 
    $(".col-md-3").animate({top:$(this).scrollTop()});
});
//Main graph animation
$('#showLineGraph').click(function() { 
	var canvasStyle = document.getElementById('canvas').style.display;
	if(canvasStyle == "none"){
		canvas.style.display="block";
	}else{
		canvas.style.display="none";
	}
});

});
