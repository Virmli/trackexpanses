//universall function to convert form data into JSON object.
$.fn.serializeObject = function() {
    var o = {}; //varaible to store an object
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}
//create ne user function which handles our form work.
/*TODO: add front end error check.
*/

$( document ).ready(function() {
    $('#register-submit').click(function(e) {
            e.preventDefault();
            
            var formData;
            formData = $('#register-form').serializeObject(); //parse form input into a json object

            //delete formData["confirm-password"]; //delete second password before put into DB
            
            var jsonFormData = JSON.stringify(formData);
              var createUser = $.ajax({
                method: "PUT",
                url: "http://52.27.200.202:3000/createUser",
                data: jsonFormData,
                contentType: "application/json",
                dataType: "json"
            });

            createUser.done(function(data) {
                //data = JSON.parse(data);
                
                $(".infoRegFirstName").hide();
                $(".infoRegLastName").hide();
                $(".infoRegEmail").hide();
                $(".infoRegPassword").hide();
                $(".infoRegUserLogin").hide();

                if(data.firstName == 1) {
                    $(".infoRegFirstName").show();
                }
                if(data.lastName == 1){
                    $(".infoRegLastName").show();
                }
                if(data.username == 1){
                    $(".infoRegUserLogin").show();
                }
                if(data.email == 1){
                    $(".infoRegEmail").show();
                }
                if(data.password == 1){
                    $(".infoRegPassword").show();
                }

                BootstrapDialog.show({
                title: "Registration message",
                message: data.msg,
                buttons: [
                    {
                        label: 'Ok',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }
                ]
                });
                //end dialog show
                
            });
            createUser.fail(function(data) {
                data = JSON.parse(data);
                console.log(data.msg);
                BootstrapDialog.show({
                title: "Registration error",
                message: data.msg,
                buttons: [
                    {
                        label: 'Ok',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }
                ]
                });
            });
    });

//login into user profile function
    $('#login-form').submit(function(e) {
    	e.preventDefault();
        var loginData;
        var resBody;
    	loginData = JSON.stringify($('#login-form').serializeObject()); //parse form input into a json object
        console.log(loginData);
    	var login = $.ajax({
    		method: "POST",
            url: "http://52.27.200.202:3000/login",
    		// The key needs to match your method's input parameter (case-sensitive).
    		data: loginData,
    		contentType: "application/json",
            dataType: "json"
        });
    		login.done(function(data) {
                console.log('success');
                console.log(data);
                $(".infoRegLogin").hide();
                if(data.login == 1){
                    $(".well").text(data.msg);
                    $(".infoRegLogin").show();
                }else{
                    //when logout need to clear local storage
                    window.localStorage.setItem("userInformation", JSON.stringify(data));
                    window.location.replace("mainProfile.html");
                }
            });
            login.fail(function(data) {
                //alert( "Login user failed: " + textStatus );
                console.log(data);
                BootstrapDialog.show({
                title: "Login Error",
                message: data,
                buttons: [
                    {
                        label: 'Ok',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }
                ]
                });
             });   
    });
}); 

//validation function
function firstNameValidation(){
    var firstName=document.getElementById('firstName').value;
    var nameRegex = "/^[A-Za-z]/";
    if (firstName =="" || firstName.length > 30 || nameRegex.test(firstName)) {
        return false;
        //show error message
    } else {
        return true;
    }
}
function lastNameValidation(){
    var lastName=document.getElementById('lastName').value;
    var nameRegex = "/^[A-Za-z]/";
    if (lastName =="" || lastName.length > 30 || nameRegex.test(lastName)) {
        return false;
        //show error message
    } else {
        return true;
    }
}

function emailValidation() {
    var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(emailRegex.test(email)){
        return true;
    }else{
       return false;
    }
}