//universall function to convert form data into JSON object.
$.fn.serializeObject = function()
{
    var o = {}; //varaible to store an object
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


$( document ).ready(function() {

    $(".no-results").text("Please create new table for this month.");




     var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];

	//set name on the overviw div
    var userInformation = JSON.parse(window.localStorage.getItem("userInformation"));
    var userName = userInformation[0].firstName + " " + userInformation[0].lastName;
    $(".profile-usertitle-name").text(userName);
    console.log(userInformation[0]._id);

    //set values to the edit profile page.
    $("#_id").val(userInformation[0]._id);
    $("#firstName").val(userInformation[0].firstName);
    $("#lastName").val(userInformation[0].lastName);
    $("#email").val(userInformation[0].email);
    $("#about").val(userInformation[0].about);
    $("#dob").val(userInformation[0].dob);

    $('#logOut').click(function(e) {
    	window.location.replace("loginRegister.html");
    	localStorage.clear();
    });

//update user basic info ajax call//
$("#updateInfo").click(function(e){
    e.preventDefault();
    var userInfo = JSON.stringify($('#updateUserInfo').serializeObject());
    var updateUser = $.ajax({
          method: "PUT",
          url: "http://52.27.200.202:3000/updateProfile",
          contentType: "application/json",
          dataType: "json",
          data: userInfo
        });
         
        updateUser.done(function( msg ) {
            BootstrapDialog.show({
                title: "Notification",
                message: "Information updated successfuly",
                buttons: [
                    {
                        label: 'Ok',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }
                ]
            });
        });
         
        updateUser.fail(function( jqXHR, textStatus ) {
          alert( "Create user failed: " + textStatus );
        });
    });
 //end update user basic info ajax call//

 //create table pop up
 $("#newTable").click(function(e){
        BootstrapDialog.show({
                title: "New Table",
               //message: 'god month, amount'+'<div class="form-group"><label for="inputDate">Date of Birth</label><div class="input-group date"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span><input type="date" class="form-control" id="dob" name="dob" placeholder="Change date"></div></div>',
                message:  '<div class="form-group">'+
                                '<label for="firtName">Monthly Amount</label>'+
                                    '<div class="input-group monthlyAmount">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span><input type="text" class="form-control" id="monthlyAmount" name="monthlyAmount" placeholder="Enter your Monthly Amount">'+
                                    '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="inputDate">Month/Year</label>'+
                                    '<div class="input-group date">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span><input type="date" class="form-control" id="datepicker" name="datepicker" placeholder="Chose grid Month/Year">'+
                                    '</div>'+
                            '</div>',
                     buttons: [
                    {
                        label: 'Create',
                        action: function(){
                            console.log(Date.now());
                            var initial_budget = $("#monthlyAmount").val();
                            var date = $("#datepicker").val();
                            var year = date.substring(0,4);
                            var monthNumber = date.substring(5,7);
                            var month;
                            console.log(date);
                            if(monthNumber.substring(0,1) == 0) {
                                month = monthNames[monthNumber.substring(1,2)-1];
                            }else {
                                month = monthNames[monthNumber-1];
                            }
                            if(initial_budget == "" || date == "" || isNaN(initial_budget)) {
                                alert("All fields are inportant to fill and Amount need to be a number");
                            }else {
                                
                                var data = '{"student_id": "'+ userInformation[0]._id+'","month":"'+month+'","year":"'+year+'","initial_budget":"'+initial_budget+'","timestamp":"'+Date.now()+'"}';
                                console.log(data);
                                var createTable = $.ajax({
                                    method: "PUT",
                                    url: 'http://52.27.200.202:3000/createGrid',
                                    contentType: "application/json",
                                    dataType: "json",
                                    data: data
                                });
                                 //fail
                                createTable.done(function() {
                                    alert("Success");
                                    location.reload();
                                });
                                 
                                createTable.fail(function() {
                                    alert( "Create user failed:");
                                });
                            }//else close
                            }
                        },
                        {
                            label: 'Cancel',
                            action: function(dialogItself){
                                dialogItself.close();
                            }
                        }
                    ]
            });
 });//end create table
$("#delete").click(function(e){
    var student_id = userInformation[0]._id;
    BootstrapDialog.show({
                    title: "Delete User",
                    message: "Are you sure you want to delete this user?",
                    buttons: [{
                        label: 'Yes',
                        action: function(dialogItself){
                            var deleteData = '{"student_id":"'+student_id+'"}';
                            console.log(deleteData);
                            var deleteAccountURL = 'http://52.27.200.202:3000/deleteProfile';
                            var deleteExpenseAjax = $.ajax({
                                method: "DELETE",
                                url: deleteAccountURL,
                                data: deleteData,
                                contentType: "application/json",
                                dataType: "json"
                            });
                           deleteExpenseAjax.done(function(data) {
                                //issue.
                                BootstrapDialog.alert('User Deleted');
                                dialogItself.close();
                                localStorage.clear();
                                window.location.replace("loginRegister.html");
                           });
                           deleteExpenseAjax.fail(function(data) {
                                BootstrapDialog.alert('Woops =)');
                           });
                        }
                    },
                    {
                        label: 'No',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                }); 
    });
});


