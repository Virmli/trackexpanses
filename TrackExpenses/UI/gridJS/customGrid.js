
function getCurrentDate(){
    var date = new Date();
    var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
    var monthYear = monthNames[date.getMonth()]+"/"+date.getFullYear();
    return monthYear;
}


$(document).ready(function(){
    var userInformation = JSON.parse(window.localStorage.getItem("userInformation"));
    console.log(userInformation);

    var userID = '{"student_id":"'+userInformation[0]._id+'"}';
    var curentGridUrl = "http://52.27.200.202:3000/"+getCurrentDate()+"/findAllExpenses";
    console.log(userID);
    var grid = $.ajax({
            method: "POST",
            url: curentGridUrl,
            data: userID,
            contentType: "application/json",
            dataType: "json"
        });
        grid.done(function(data) {

            window.localStorage.setItem("monthlyGridInformation", JSON.stringify(data));//SET GRID DATA TO A LOCAL STORAGE
            console.log("Grid-data is loaded");
            var monthlyBudget = data.monthlyBudget;
            var monthYear = data.month +"/"+ data.year;

            //var availableFundsPos = Object.keys(data.rows).length - 1;
            var availableFunds = data.availableFunds;

            $("#monthYear").text(monthYear);
            $("#monthlyBudget").text("$"+monthlyBudget);
            $("#availableFunds").text("$"+availableFunds);

            //console.log(Object.keys(data.rows).length);
            $("#grid-data").bootgrid("append", data.rows);
            var checkAvailebleFunds = $("#availableFunds").text();
            if(checkAvailebleFunds.indexOf('-') != -1){
                $("#avFunds").css("color", "#CA4242");
            }
        });

        grid.fail(function(){
            BootstrapDialog.show({
                title: "Table Error",
                message: "Table for current month does not exist. Please Create New Table",
                buttons: [
                    {
                        label: 'Ok',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }
                ]
        });
        });
 
        var gridButtons = $("#grid-data").bootgrid({
            formatters: {
                "commands": function(column, row) {
                        return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""+ row._id  + "\"><span class=\"glyphicon glyphicon-pencil\"></span></button> " + 
                               "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\""+ row._id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
                }
     
            }
        }).on("loaded.rs.jquery.bootgrid", function() {
            /* Executes after data is loaded and rendered */
            gridButtons.find(".command-edit").on("click", function(e) {
                var rowID = $(this).data("row-id");
                var columnNumber = 0;
                var gridObject = $('#grid-data').bootgrid().data('.rs.jquery.bootgrid');
                var rowsObject = gridObject.currentRows;

                   for(var i=0; i< Object.keys(rowsObject).length; i++){
                        if(rowsObject[i]._id == rowID){
                            columnNumber = i;
                        }
                   }

                 BootstrapDialog.show({
                    title: "Update your record",
                    message:  '<div class="form-group">'+
                                '<label for="expanseName">Expanse Name</label>'+
                                    '<div class="input-group expanseName">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" id="expanseName" name="expanseName" placeholder="Enter Expanse Name">'+
                                    '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="amount">Amount</label>'+
                                    '<div class="input-group date">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span><input type="text" class="form-control" id="amount" name="amount" placeholder="How much you spend today?">'+
                                    '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="inputDetails">Details</label>'+
                                    '<div class="input-group details">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span><textarea class="form-control" id="details" name="details" placeholder="What did you buy today?"></textarea>'+
                                    '</div>'+
                            '</div>'+
                            '<script>$("#expanseName").val("'+rowsObject[columnNumber].name+'");'+
                                    '$("#amount").val("'+rowsObject[columnNumber].amount+'");'+
                                    '$("#details").val("'+rowsObject[columnNumber].details+'");'+
                            '</script>',
                    buttons: [{
                        label: 'Update',
                        action: function(){
                            //UPDATE RECORD IN GRID
                            var updateData = '{"amount":"'+$("#amount").val()+'","name":"'+$("#expanseName").val()+'","details":"'+$("#details").val()+'"}';

                            console.log(rowsObject);
                            var updateUrl = 'http://52.27.200.202:3000/'+rowsObject[columnNumber]._id+'/updateExpense';
                            var updateAjax = $.ajax({
                                method: "POST",
                                url: updateUrl,
                                data: updateData,
                                contentType: "application/json",
                                dataType: "json"
                            });

                            updateAjax.done(function(data) {
                                 BootstrapDialog.show({
                                    title: "Update Ok",
                                    message: "Record Updated!",
                                    buttons: [
                                        {
                                            label: 'Ok',
                                            action: function(dialogItself){

                                               dialogItself.close();
                                               location.reload();
                                            }
                                        }
                                    ]
                                });
                            });

                            updateAjax.fail(function(){
                                BootstrapDialog.show({
                                    title: "Update Error",
                                    message: "Woops =)",
                                    buttons: [{
                                            label: 'Ok',
                                            action: function(dialogItself){
                                                dialogItself.close();
                                            }
                                        }]
                                });    
                            });
                        }
                    },
                    {
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                }); 
            }).end().find(".command-delete").on("click", function(e){
                //DELETE RECORD FROM GRID
                var expenseID = $(this).data("row-id");
                BootstrapDialog.show({
                    title: "Delete Record",
                    message: "Are you sure you want to delete this record?",
                    buttons: [{
                        label: 'Yes',
                        action: function(dialogItself){
                            var deleteEpenseURL = 'http://52.27.200.202:3000/'+expenseID+'/deleteExpense';
                           var deleteExpenseAjax = $.ajax({
                                method: "DELETE",
                                url: deleteEpenseURL
                                //data: updateData,
                               // contentType: "application/json",
                               // dataType: "json"
                            });
                           deleteExpenseAjax.done(function(data) {
                                BootstrapDialog.alert('Record Deleted successfully');
                                dialogItself.close();
                                location.reload();
                           });
                           deleteExpenseAjax.fail(function(data) {
                                BootstrapDialog.alert('Woops =)');
                           });
                        }
                    },
                    {
                        label: 'No',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                });    
            });//end grid buttons
        });
//////////////////ADD RECORD TO THE GRID
$("#addNewRecord").click(function(){
    var student_id = userInformation[0]._id;
    var available_funds = $("#availableFunds").text().slice(1);
    var monthYear = $("#monthYear").text();
    var day = new Date().getDate();

    BootstrapDialog.show({
                    title: "ADD new record",
                    message:  '<div class="form-group">'+
                                '<label for="expanseName">Expanse Name</label>'+
                                    '<div class="input-group expanseName">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" id="expanseName" name="expanseName" placeholder="Enter Expanse Name">'+
                                    '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="amount">Amount</label>'+
                                    '<div class="input-group date">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span><input type="text" class="form-control" id="amount" name="amount" placeholder="How much you spend today?">'+
                                    '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="inputDetails">Details</label>'+
                                    '<div class="input-group details">'+
                                        '<span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span><textarea class="form-control" id="details" name="details" placeholder="What did you buy today?"></textarea>'+
                                    '</div>'+
                            '</div>',
                    buttons: [{
                        label: 'Add record',
                        action: function(dialogItself){
                                var addData = '{ "student_id":"'+student_id+
                                          '","month" : "","year" : "", "date" : "'+day+'",'+
                                          '"amount" : "'+$("#amount").val()+
                                          '","name" : "'+$("#expanseName").val()+
                                          '","details":"'+$("#details").val()+
                                          '","available_funds" : "'+available_funds+'"}';
                                          console.log(addData);
                                var updateRecordURL = "http://52.27.200.202:3000/"+monthYear+"/createExpense";
                                var updateRecordAjax = $.ajax({
                                    method: "PUT",
                                    url: updateRecordURL,
                                    data: addData,
                                    contentType: "application/json",
                                    dataType: "json"
                                });

                                updateRecordAjax.done(function(data) {
                                    BootstrapDialog.alert('Record created');
                                    dialogItself.close();
                                    location.reload();
                                });
                                updateRecordAjax.fail(function(){
                                    BootstrapDialog.alert('Woops =)');
                                });
                        }
                    },
                    {
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                    }
                }]
            });    
});//END ADD RECORDS TO THE GRID
});