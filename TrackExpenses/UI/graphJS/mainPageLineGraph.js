var currentMonthGridData = localStorage.getItem("monthlyGridInformation");
currentMonthGridData = JSON.parse(currentMonthGridData);


$(document).ready(function() {
console.log("THis data from linegraph page");   
console.log(currentMonthGridData);

    var lineChartData = {

        labels : labelsFrontGraph(),
        datasets : [
            {
                label: "September",
                fillColor : "rgba(220,220,220,0.6)",
                strokeColor : "#d9534f",
                pointColor : "#42ACCA",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : amountFrontGraph()
            }
        ]
    }
var options = {
    // String - Template string for single tooltips
    tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= 'You spend '+ value + ' $' + ' in ' + currentMonthGridData.rows[0].name %>", //add dinamic place
    // String - Template string for multiple tooltips
    multiTooltipTemplate: "<%= value + ' %' %>"
};

   //window.onload = function(){
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, options,{
            responsive: true,
            pointDotRadius : 6
        });
    //}
});

//Polar Are graph
    var polarData = [
                {
                    value: 500,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 244,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 300,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                },
                {
                    value: 0,
                    color: "#949FB1",
                    highlight: "#A8B3C5",
                    label: "Grey"
                },
                {
                    value: 50,
                    color: "#4D5360",
                    highlight: "#616774",
                    label: "Dark Grey"
                }

            ];
    var polarOptions = {
           scaleBeginAtZero : true
    };
            window.onload = function(){
                var ctxPolar = document.getElementById("chart-area").getContext("2d");
                window.myPolarArea = new Chart(ctxPolar).PolarArea(polarData, polarOptions, {
                    responsive:true
                });
            };

//end donught graph
/*
    Functions for line graph, by month.
*/
//returns lower axis labels Day: Day Number
function labelsFrontGraph(){
    var labels = [];
    var weekDays= ["Sun","Mon","Tue","Wed","Thur","Fri","Sat","Sun"]
    for(var n=0; n <= currentMonthGridData.rows.length-1; n++){
        
        var dateString = "";
        dateString += currentMonthGridData.rows[n].month + " " + currentMonthGridData.rows[n].date + " " + currentMonthGridData.rows[n].year;
        var date = new Date(dateString);

        labels.push(weekDays[date.getDay()]+" " + currentMonthGridData.rows[n].date);
    }
    return labels;
}
//returns the total array of spendigs for current user
function amountFrontGraph(){
    var amounts = [];
    for(var n=0; n <= currentMonthGridData.rows.length-1; n++){
        amounts.push(currentMonthGridData.rows[n].amount);
    }
    return amounts;
}