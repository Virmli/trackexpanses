# README #

### What is this repository for? ###
Few words about my project;
Im working on this project in my free time. the idea was to create a web-application which can help tack people`s expenses. Users can login, write there expenses and see the history by month what they bought and how much they spend. I used Node.js for Back-end REST server, MongoDB as my Data storage and JS, Jquery, Bootstrap and some more JS libraries for front end. This project is far from end and has lots of bugs which can brake hole application.

### How do I get set up? ###
Currently this repository doesn't have a DB backup, as a result all my database located on the AWS. Also my server is running 24/7 except days when I work on this project.
These 2 things makes setup super easy:
##
* 1) You need to pull my project
* 2) Navigate to TrackExpenses/UI/loginRegister.html
* 3) Application is running

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* My name is Maksym Syvozhelizov
* The best way to contact me by email: maksym.siv@gmail.com